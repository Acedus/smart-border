**Smart Border IT Issue Tracker**
----

Welcome to the official IT department issue tracker repo.
To create an issue, navigate to issues, and press "new issue".

An example issue should be created as so:

---


**Title:** I need to open some ports.

**Description:** 

```
Unit: 1

Team: Mobile

Issue:

Hi, I need to open ports 3000, 3086, and 8000 on the mobile01 VM, thanks!
```

---

Once done click create an issue, and one of the team members will resolve the issue in a timely manner.


